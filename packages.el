(setq w3m-packages '(w3m))

(defun w3m/init-w3m ()
  (use-package w3m
    :defer t
    :init
    (progn
      (require 'w3m-search))
    :config
    (progn
      (evilified-state-evilify w3m-mode w3m-mode-map
        (kbd "S") 'w3m-search-startpage
        (kbd "ss") 'w3m-search-startpage
        (kbd "st") 'w3m-search-thesaurus
        (kbd "sw") 'w3m-search-wiki
        (kbd "{") 'backward-paragraph
        (kbd "}") 'forward-paragraph
        (kbd "d") 'evil-delete ; won't work, why?
        (kbd "D") 'evil-delete-line ; same
        (kbd "f") 'w3m-external-view-current-url
        (kbd "y") 'w3m-print-current-url
        (kbd "Y") 'w3m-print-this-url)
      (define-key w3m-mode-map (kbd "d") 'evil-delete)
      (define-key w3m-mode-map (kbd "D") 'evil-delete-line)
      (setq w3m-fill-column 64)
      (setq browse-url-browser-function 'w3m-browse-url)
      (setq w3m-use-tab nil)
      (setq w3m-use-tab-menubar nil)
      (setq w3m-search-engine-alist
            '(("startpage" "https://startpage.com/do/search?query=%s&cat=web&pl" utf-8)
              ("tfd" "https://tfd.com/%s" utf-8)
              ("en.wikipedia" "https://en.wikipedia.org/wiki/Special:Search?search=%s")))
      (setq w3m-seach-default-engine "startpage"))))

(defun w3m-search-scroogle (search-engine query)
  (interactive (w3m-search-read-variables))
  (w3m-search-do-search 'w3m-goto-url "scroogle" query))

(defun w3m-search-startpage (search-engine query)
  (interactive (w3m-search-read-variables))
  (w3m-search-do-search 'w3m-goto-url "startpage" query))

(defun w3m-search-wiki (search-engine query)
  (interactive (w3m-search-read-variables))
  (w3m-search-do-search 'w3m-goto-url "en.wikipedia" query))

(defun w3m-search-thesaurus (search-engine query)
  (interactive (w3m-search-read-variables))
  (when (boundp 'remember-thesaurus-search)
    (remember-thesaurus-search query))
  (w3m-search-do-search 'w3m-goto-url "tfd" query))
